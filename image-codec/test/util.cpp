#include <catch2/catch.hpp>
#include <codec/util.hpp>

TEST_CASE("meta data in headers can be written and read properly", "[util][meta_data_read_write]") {
    {
        bool const use_rle = true;
        std::vector<int> const quant_matrix{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        std::size_t const width = 12345;

        std::vector<int> quant_matrix_cp = quant_matrix;
        CHECK(
            codec::read_metadata(
                codec::generate_metadata(std::move(quant_matrix), width, use_rle)) ==
            std::make_tuple(quant_matrix_cp, width, use_rle));
    }
    {
        bool const use_rle = false;
        std::vector<int> const quant_matrix{
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10000, 11, 12, 13, 255, 0};
        std::size_t const width = 0;

        std::vector<int> quant_matrix_cp = quant_matrix;
        CHECK(
            codec::read_metadata(
                codec::generate_metadata(std::move(quant_matrix), width, use_rle)) ==
            std::make_tuple(quant_matrix_cp, width, use_rle));
    }
}

TEST_CASE("coefficients can be written and read properly", "[util][coefficients_read_write]") {
    std::vector<int> image{123, 125, -23, 65, 34,  12, 23, 3,   4,  -213, 153, 123, 57, 234,
                           -23, -32, 234, 23, 223, 42, 34, 234, 46, 34,   234, 34,  23};

    auto const image_cp = image;
    CHECK(codec::read_coefficients(codec::write_coefficients(std::move(image))) == image_cp);
}

TEST_CASE("RLE components can be written and read properly", "[util][components_read_write]") {
    codec::rle_components components{
        {0, 123},  {6, 125}, {0, -23}, {2, 65},  {0, 34},  {5, 12},  {1, 23},   {0, 3},   {0, 4},
        {2, -213}, {6, 153}, {0, 123}, {2, 57},  {0, 234}, {5, -23}, {0, 0},    {0, 234}, {0, 23},
        {2, 223},  {6, 42},  {0, 0},   {2, 234}, {0, 46},  {7, 34},  {15, 234}, {0, 34},  {0, 0}};

    auto components_cp = components;

    auto buffer = codec::write_rle_coefficients(std::move(components));
    auto result = codec::read_rle_coefficients(std::move(buffer));

    CHECK(result == components_cp);
}
