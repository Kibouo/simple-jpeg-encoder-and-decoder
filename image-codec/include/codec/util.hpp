#pragma once

#ifndef UTIL_H
#define UTIL_H

#include "codec/codec.hpp"
#include <filesystem>
#include <fstream>
#include <vector>

namespace codec {
    int const RLE_USED_SIZE = 1;
    int const QUANT_MATRIX_SIZE = 64;
    int const IMAGE_WIDTH_SIZE = 4;
    int const META_DATA_SIZE = RLE_USED_SIZE + QUANT_MATRIX_SIZE + IMAGE_WIDTH_SIZE;

    std::vector<int> matrix_from_file(std::filesystem::path const &filepath);
    std::vector<char> read_file(std::filesystem::path const &path);
    /// Writes header data (quant matrix, image width, etc.) to a buffer. Layout:
    /// [1B rle used][64B(4*4*4B) quant matrix][4B image width]
    std::vector<char> generate_metadata(
        std::vector<int> const &&quantisation_matrix,
        std::size_t const width,
        bool const use_rle);
    /// Read header data (quant matrix, image width, etc.) from a buffer.
    std::tuple<std::vector<int> const, std::size_t, bool> read_metadata(std::vector<char> &&header);
    /// Writes image without rle encoding into buffer.
    std::vector<char> const write_coefficients(std::vector<int> &&image);
    /// Reads image without rle encoding from buffer.
    std::vector<int> read_coefficients(std::vector<char> const &&buffer);
    /// Writes RLE encoded data into a buffer. Layout:
    /// [4bits amt 0's, 2B coefficient]
    std::vector<char> write_rle_coefficients(codec::rle_components const &&components);
    /// Reads RLE encoded data from a buffer.
    codec::rle_components read_rle_coefficients(std::vector<char> &&file);
    std::vector<char> const vec_uint8_into_vec_char(std::vector<std::uint8_t> &&data);
    template <typename T>
    void
    log_block(std::vector<T> const &image, std::ofstream &logfile, std::size_t block_offset = 0) {
        logfile << std::string("BLOCK NR.") << std::to_string(block_offset) << std::endl;

        for (size_t i = 0; i < BLOCK_SIZE; i++) {
            for (size_t j = 0; j < BLOCK_SIZE; j++) {
                logfile << std::to_string(image[block_offset + i * BLOCK_SIZE + j])
                        << std::string(", ");
            }
            logfile << std::endl;
        }
    }
} // namespace codec

#endif