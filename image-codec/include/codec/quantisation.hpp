#pragma once

#ifndef QUANTISATION_H
#define QUANTISATION_H

#include <optional>
#include <vector>

namespace codec {
    /**
     * Quantise a vector of floats.
     * @param image A vector of floats to quantise.
     * @param factors The quantisation factors to quantise with.
     * @return A vector or integers containing the result of the quantisation.
     */
    std::vector<int> quantise(
        std::vector<float> const &image,
        std::vector<int> const &factors,
        std::optional<std::ofstream> &logfile);

    /**
     * Undoes the quantisation of a vector.
     * @param image A vector of integers to reverse the quantisation of.
     * @param factors The quantisation factors the vector was quantised with.
     * @return A vector of integers containing an approximation of the original vector before
     * quantisation.
     */
    std::vector<float> dequantise(std::vector<int> const &image, std::vector<int> const &factors);
} // namespace codec

#endif // QUANTISATION_H