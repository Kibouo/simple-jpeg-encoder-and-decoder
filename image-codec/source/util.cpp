#include "codec/util.hpp"
#include "codec/BitStream.h"
#include <fstream>
#include <functional>

namespace codec {
    int const BYTES_TAKEN_BY_HUFFMAN_TABLES_SIZE = 2;
    int const TABLE_MARKER_SIZE = 1;
    int const AMT_HUFFMAN_CODES_SIZE = 16;
    int const VALUE_SIZE = 2;
    int const DATA_SIZE = 4;
    int const AMT_ZEROS_BITS_SIZE = 4;
    constexpr int const COEFFICIENT_BITS_SIZE = 8 * sizeof(coefficient_t);

    std::vector<int> matrix_from_file(std::filesystem::path const &filepath) {
        std::ifstream file(filepath);

        std::string line;
        std::string cell;
        std::vector<int> matrix_raw;

        while (!file.eof()) {
            int value;
            file >> value;
            if (!file.fail() || file.bad())
                matrix_raw.push_back(value);
        }

        if ((file.fail() || file.bad()) && !file.eof())
            throw std::runtime_error("error reading matrix file");

        return matrix_raw;
    }

    std::vector<char> read_file(std::filesystem::path const &path) {
        std::ifstream file(path, std::ios::binary | std::ios::in);
        file.exceptions(std::ios::failbit | std::ios::badbit);

        return std::vector<char>(
            std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
    }

    std::vector<char> generate_metadata(
        std::vector<int> const &&quantisation_matrix,
        std::size_t const width,
        bool const use_rle) {

        std::vector<uint8_t> buffer(META_DATA_SIZE);
        util::BitStreamWriter writer(buffer.data(), META_DATA_SIZE);
        writer.put(8, use_rle);
        for (auto const &quant_factor : quantisation_matrix)
            writer.put(8 * QUANT_MATRIX_SIZE / codec::MATRIX_SIZE, quant_factor);
        writer.put(8 * IMAGE_WIDTH_SIZE, width);

        return vec_uint8_into_vec_char(std::move(buffer));
    }

    std::tuple<std::vector<int> const, std::size_t, bool>
    read_metadata(std::vector<char> &&header) {
        // convert char's to uint8's
        std::vector<uint8_t> buffer{std::make_move_iterator(header.begin()),
                                    std::make_move_iterator(header.end())};

        util::BitStreamReader reader(buffer.data(), buffer.size());
        bool const rle_used = reader.get(8);
        std::vector<int> quant_matrix{};
        for (size_t i = 0; i < codec::MATRIX_SIZE; i++)
            quant_matrix.push_back(reader.get(8 * QUANT_MATRIX_SIZE / codec::MATRIX_SIZE));
        std::size_t const width = reader.get(8 * IMAGE_WIDTH_SIZE);

        return {quant_matrix, width, rle_used};
    }

    std::vector<char> const write_coefficients(std::vector<int> &&image) {
        auto const image_size = image.size();
        std::vector<std::int16_t> const result(
            std::make_move_iterator(image.begin()), std::make_move_iterator(image.end()));
        return std::vector<char>(
            reinterpret_cast<char const *>(result.data()),
            reinterpret_cast<char const *>(result.data()) + image_size * sizeof(std::int16_t));
    }

    std::vector<int> read_coefficients(std::vector<char> const &&buffer) {
        std::vector<std::int16_t> result(
            reinterpret_cast<std::int16_t const *>(buffer.data()),
            reinterpret_cast<std::int16_t const *>(buffer.data()) +
                buffer.size() / sizeof(std::int16_t));
        return {std::make_move_iterator(result.begin()), std::make_move_iterator(result.end())};
    }

    std::vector<char> write_rle_coefficients(codec::rle_components const &&components) {
        auto const buffer_size = std::ceil(
            (float)(components.size()) * (float)(AMT_ZEROS_BITS_SIZE + COEFFICIENT_BITS_SIZE) /
            8.0);
        std::vector<std::uint8_t> coefs_u8(buffer_size);
        util::BitStreamWriter coefs_writer(coefs_u8.data(), buffer_size);
        for (auto const &[zeros, coef] : components) {
            coefs_writer.put(AMT_ZEROS_BITS_SIZE, std::move(zeros));
            coefs_writer.put(COEFFICIENT_BITS_SIZE, std::move(coef));
        }
        std::vector<char> coef_char = vec_uint8_into_vec_char(std::move(coefs_u8));

        return coef_char;
    }

    codec::rle_components read_rle_coefficients(std::vector<char> &&file) {
        // convert char's to uint8's
        std::vector<uint8_t> buffer{std::make_move_iterator(file.begin()),
                                    std::make_move_iterator(file.end())};
        util::BitStreamReader reader(buffer.data(), buffer.size());

        codec::rle_components components{};
        auto const bits_left_in_buffer = reader.get_size() * 8 - reader.get_position();
        auto const tuples_left_to_read =
            bits_left_in_buffer / (AMT_ZEROS_BITS_SIZE + COEFFICIENT_BITS_SIZE);
        for (std::size_t i = 0; i < tuples_left_to_read; i++) {
            std::uint8_t zeros = reader.get(AMT_ZEROS_BITS_SIZE);
            coefficient_t coef = reader.get(COEFFICIENT_BITS_SIZE);
            components.push_back({zeros, coef});
        }

        return components;
    }

    std::vector<char> const vec_uint8_into_vec_char(std::vector<std::uint8_t> &&data) {
        std::vector<char> result;
        result.reserve(data.size() * sizeof(char));
        for (uint8_t const byte : data)
            result.push_back((unsigned char)(std::move(byte)));
        return result;
    }
} // namespace codec
