#include "codec/codec.hpp"
#include "codec/dct.hpp"
#include "codec/quantisation.hpp"
#include "codec/util.hpp"
#include <algorithm>
#include <fstream>
#include <iterator>
#include <limits>
#include <sstream>
#include <string>

namespace codec {

    /**
     * Splits a line on whitespaces.
     */
    std::vector<std::string> const split_line(std::string const &line) {
        std::istringstream line_stream(line);
        std::vector<std::string> split_line(
            (std::istream_iterator<std::string>(line_stream)),
            std::istream_iterator<std::string>());
        return split_line;
    }

    rle_components run_length_encoding(std::vector<coefficient_t> const &zigzagged_blocked_image) {
        std::uint8_t zeros_seen = 0;
        rle_components encoded{};

        for (size_t i = 0; i < zigzagged_blocked_image.size(); i++) {
            // When encountering the start of a new block, insert a (0,0) to indicate the end of
            // the previous block.
            if ((i % MATRIX_SIZE == 0) && zeros_seen != 0) {
                zeros_seen = 0;
                encoded.push_back({0, 0});
            }
            if (zigzagged_blocked_image.at(i) == 0)
                zeros_seen++;
            else {
                encoded.push_back({zeros_seen, zigzagged_blocked_image.at(i)});
                zeros_seen = 0;
            }
        }

        // tuple to indicate the end of the last block.
        if (zeros_seen != 0)
            encoded.push_back({0, 0});
        return encoded;
    }

    std::vector<coefficient_t> run_length_decoding(rle_components const &&data) {
        std::vector<coefficient_t> components{};

        for (auto const &[zeros, coef] : data) {
            // - normal component
            if (zeros != 0 || coef != 0) {
                for (size_t zero_index = 0; zero_index < zeros; zero_index++) {
                    components.push_back(0);
                }
                components.push_back(coef);
            }

            // - special (0,0) tuple
            else if (zeros == 0 && coef == 0) {
                components.push_back(0);
                while ((components.size() % MATRIX_SIZE) != 0) {
                    components.push_back(0);
                }
            }
        }

        return components;
    }

    std::vector<char> encode(
        std::vector<char> const &&raw_image,
        std::size_t const width,
        std::vector<int> const &quantisation_matrix,
        bool const use_rle,
        std::optional<std::ofstream> &logfile) {

        std::vector<int> image;
        image.reserve(raw_image.size());

        for (char const ch : raw_image)
            image.push_back((unsigned char)(std::move(ch)));

        {
            image = image_to_blocks<int>(std::move(image), BLOCK_SIZE, BLOCK_SIZE, width);
            image = dct_and_quantise(std::move(image), quantisation_matrix, logfile);
            image = zigzag(std::move(image));
        }

        auto result = generate_metadata(std::move(quantisation_matrix), width, use_rle);
        std::vector<char> data{};
        if (use_rle) {
            data = write_rle_coefficients(run_length_encoding(std::vector<std::int16_t>(
                std::make_move_iterator(image.begin()), std::make_move_iterator(image.end()))));
        } else
            data = write_coefficients(std::move(image));

        result.insert(
            result.cend(),
            std::make_move_iterator(data.begin()),
            std::make_move_iterator(data.end()));

        return result;
    }

    std::vector<char> const decode(std::vector<char> &&encoded_image_with_metadata) {

        std::vector<char> metadata{
            std::make_move_iterator(encoded_image_with_metadata.begin()),
            std::make_move_iterator(encoded_image_with_metadata.begin() + codec::META_DATA_SIZE)};
        std::vector<char> encoded_image{
            std::make_move_iterator(encoded_image_with_metadata.begin() + codec::META_DATA_SIZE),
            std::make_move_iterator(encoded_image_with_metadata.end())};

        auto const [quantisation_factors, width, use_rle] =
            codec::read_metadata(std::move(metadata));

        std::vector<int> image{};
        if (use_rle) {
            auto data = run_length_decoding(read_rle_coefficients(std::move(encoded_image)));
            for (auto const &value : data)
                image.push_back((int)(std::move(value)));
        } else
            image = read_coefficients(std::move(encoded_image));

        {
            image = undo_zigzag(std::move(image));
            image = undo_dct_and_quantise(std::move(image), quantisation_factors);
            image = blocks_to_image<int>(std::move(image), BLOCK_SIZE, BLOCK_SIZE, width);
        }

        std::vector<char> result;
        result.reserve(image.size());

        constexpr int const min = std::numeric_limits<unsigned char>::min();
        constexpr int const max = std::numeric_limits<unsigned char>::max();

        for (int const element : image)
            result.push_back((unsigned char)(std::clamp(std::move(element), min, max)));

        return result;
    }

    std::vector<int> dct_and_quantise(
        std::vector<int> &&image,
        std::vector<int> const &factors,
        std::optional<std::ofstream> &logfile) {
        return quantise(
            dct(std::vector<float>(
                    std::make_move_iterator(image.begin()), std::make_move_iterator(image.end())),
                logfile),
            factors,
            logfile);
    }

    std::vector<int>
    undo_dct_and_quantise(std::vector<int> &&image, std::vector<int> const &factors) {
        std::vector<float> result(reverse_dct(dequantise(image, factors)));
        return std::vector<int>(
            std::make_move_iterator(result.begin()), std::make_move_iterator(result.end()));
    }
} // namespace codec
