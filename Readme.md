# Grayscale Image Compression Codec

## Compiling

```bash
mkdir build
cd build
CXX=clang++ CC=clang cmake -DCMAKE_BUILD_TYPE=Release ..
make -j
cd ..
```

## Testing

```bash
cd build
make test
cd ..
```

## Running

```bash
cd build
./image-encoder/image-encoder --help
# ./image-encoder/image-encoder <config.conf> [config-overrides]
./image-decoder/image-decoder --help
# ./image-encoder/image-decoder <config.conf> [config-overrides]
cd ..
```

## Implementation

We implemented the following features in our codec:

1. Direct Cosine Transform
2. Quantisation
3. Zig-zagging and simple RLE (4 bits for zero-count, 16 bits for amplitude)
4. Logging in the encoder

We implemented everything required by the assignment. We used the following libraries to accomplish these.

- CLI11 for command line and config file argument parsing. We edited this library to our needs.
- Eigen for matrix calculations in the direct consine transform stage.
- Catch2 for unit tests.

We encountered a few issues while implementing the codec, all of which were resolved.

- C++
- The raw files read unsigned single bytes for every pixel. We used signed bytes, which due to 2s compliment did not give identical results.
- We forgot to do dequantisation at some point.

### Project layout

```
image-codec/        # codec library used by encoder and decoder
    include/           # header files for codec
    source/            # source code for codec
    test               # unit tests
image-decoder/      # decoder application
    include/           # header files for decoder
    source/            # source files for decoder
image-encoder/      # encoder application
    include/            # header files for encoder
    source/             # source files for encoder
libraries/          # code of used libraries
    Catch2/             # Catch2 library for unit testing
    CLI11/              # CLI11 library for CLI and conf parsing
    Eigen/              # Eigen library for matrix operations
Readme.md           # <-- you are here
```
